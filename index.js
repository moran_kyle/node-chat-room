var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var onlineUsers = [];
var typingUsers = [];

app.get('/', function(req, res){ //Adds the route for / to return index.html
	res.sendFile(__dirname+'/index.html');
});


io.on('connection', function(socket){
	
	

	socket.on('chat-message', function(msg){
		if(msg=="" || msg==" "){

		}else{
			console.log(socket.nick+": "+msg);
			io.emit('chat-message', { message:msg, userNickname:socket.nick});
		}
	});

	socket.on('nick-create', function(nick){
		console.log(nick+" has joined the chat");
		console.log("User '"+nick+"' has been added.");
		io.emit("chat-message",{message:"User '"+nick+"' has joined.", userNickname:"SERVER"});
		socket.nick = nick;
		onlineUsers.push(nick);
		io.emit("online-users", onlineUsers);
	});

	socket.on("disconnect", function(){
		console.log("User '"+socket.nick+"' has left");
		io.emit("chat-message",{message:"User '"+socket.nick+"' has left", userNickname:"SERVER"});
		onlineUsers.splice(onlineUsers.indexOf(socket.nick), 1);
		io.emit('online-users',onlineUsers);
	});

	socket.on('nick-change', function(nick){
		io.emit('chat-message', { message:"User '"+socket.nick+"' has changed to '"+nick+"'", userNickname:"SERVER"});
		socket.nick = nick;
		onlineUsers.splice(onlineUsers.indexOf(socket.nick), 1);
		onlineUsers.push(socket.nick);
		console.log(onlineUsers);
		io.emit("online-users", onlineUsers);
	});

	socket.on('is-typing', function(nickname){
		if(typingUsers.indexOf(nickname) == -1){
			typingUsers.push(nickname);
			io.emit('update-typing', typingUsers);
		}
	});

	socket.on('isnt-typing', function(nickname){
		typingUsers.splice(typingUsers.indexOf(nickname), 1);
		io.emit('update-typing', typingUsers);
	});


});





http.listen(8081, function(){
	console.log("Web app started on port 8081");
})
